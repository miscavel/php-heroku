<?php

require('vendor/autoload.php');

use PostgreSQL\Connection as Connection;
use PostgreSQL\AccountDB as AccountDB;
 
try 
{
    $pdo = Connection::get()->connect();
    //echo 'A connection to the PostgreSQL database sever has been established successfully.';

    $accountDB = new AccountDB($pdo);
    $accounts = $accountDB->all();
} 
catch (\PDOException $e) 
{
    echo $e->getMessage();
}
	
?>

<!DOCTYPE html>
<html>
    <head>
        <title>PostgreSQL PHP Querying Data Demo</title>
        <link rel="stylesheet" href="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.css">
    </head>
    <body>
        <div class="container">
            <h1>Accounts</h1>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>E-mail</th>
                        <th>Created On</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($accounts as $account) : ?>
                        <tr>
                            <td><?php echo htmlspecialchars($account['user_id']) ?></td>
                            <td><?php echo htmlspecialchars($account['username']); ?></td>
                            <td><?php echo htmlspecialchars($account['password']); ?></td>
                            <td><?php echo htmlspecialchars($account['email']); ?></td>
                            <td><?php echo htmlspecialchars($account['created_on']); ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </body>
</html>